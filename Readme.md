# Upswell Cloud, Updater

The Updater application provides a CLI to that can be incorporated into applications to provide autoupdate functionality.

The principal is simple, the application to be updated launches the Updater as a detached subprocess, providing the URL with the replacement bundle, and the installed application directory to be replaced. The Updater will download the bundle and replace the contents of the installed application directory. Optionally, if provided the application be relaunched automatically by providing the `--exe` and `--args` command line arguments.

The Updater is part of the [Upswell Cloud](https://upswell.cloud) common utility repository providing simple, reusable tools for deploying on-site media experiences.

## Usage

```bash
Arguments:
    URL     The URL of the bundle to download.
    DEST    The absolute path to the destination folder.

Usage:
    updater.py [URL] [DEST]
    updater.py [URL] [DEST] --exe App.exe
    updater.py [URL] [DEST] --exe App.exe --args '--version'
    updater.py (-h | --help)
    updater.py --version

Options:
    --args=<args>   (optional) The source folder if different than destination.
    --exe=<exe>     The absolute path the executable to restart.
    -h --help       Show this screen.
    --version       Show version.
```

## Development

### Dependencies

The following tools are required for development:

* Python 3
* PIP
* Virtual Environment

### Quickstart

```bash
cd ../xot-updater
virtualenv -p python3 venv
source venv/bin/activate
pip3 install -r requirements.txt
```

### Package Application

The Updater application is packaged as a stan-alone, cross-platform application
using PyInstaller.

To build the application for a given platform, run:

```bash
pyinstaller --clean -y updater.py
```

**Note**: PyInstaller must be run on each target platform to build a compatible binary.

## About Upswell

Upswell is an independent creative partner to forward-thinking leaders tackling the complex challenges facing our world today.

We combine storytelling with creative applications of technology to create experiences that stimulate different ways of seeing, understanding, and engaging with the urgent topics of our time.

## About the Upswell Cloud

The Upswell Cloud provides organizations with the tools needed to manage the day-to-day development, deployment and operation of physical media spaces.

For more information contact Upswell's Director of Technology, [Kieran Lynn](mailto:kieran@hello-upswell.com).