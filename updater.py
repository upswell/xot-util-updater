"""Upswell Cloud Updater

A generic updater to download bundle from remote server and replace it on the
local computer.

Arguments:
    URL     The URL of the bundle to download.
    DEST    The absolute path to the destination folder.

Usage:
    updater.py [URL] [DEST]
    updater.py [URL] [DEST] --exe App.exe
    updater.py [URL] [DEST] --exe App.exe --args '--version'
    updater.py (-h | --help)
    updater.py --version

Options:
    --args=<args>   A quoted string of command line arguments to provide to the
        aplication when restarting.
    --exe=<exe>     The absolute path the executable to restart.
    -h --help       Show this screen.
    --version       Show version.
"""
import os
import requests
import sys
import zipfile
import shutil

from appdirs import user_data_dir
from colorama import init, Fore
from docopt import docopt


def cleanup(files, folders):
    """Cleanup

    Remove all of the extraneous files downloaded by the updater.

    Args:
        files (list): List of files to clean-up
        folders (list): List of folders to clean-up
    """
    print(Fore.CYAN + "[5] Cleanup")

    for f in files:
        try:
            os.remove(f)
            print("    - Remove `%s`" % f)
        except FileNotFoundError:
            print(Fore.YELLOW + "    - Cannot remove missing file `%s`" % f)

    for f in folders:
        try:
            shutil.rmtree(f)
            print("    - Remove `%s`" % f)
        except FileNotFoundError:
            print(Fore.YELLOW + "    - Cannot remove missing file `%s`" % f)


def download_bundle(uri, fn):
    """Download Bundle

    Download the zip bundle with the new version of the application.

    Args:
        uri (str): The URI of the bundle to download.

    Returns:
        (bool): Boolean indicating success or failure.
    """

    print(Fore.CYAN + "[2] Download Application Bundle")
    print("    - Download `%s`" % uri)
    print("    - Destination `%s`" % fn)

    with open(fn, "wb") as f:
        r = requests.get(uri, stream=True)

        if r.status_code == 200:
            total_length = r.headers.get('content-length')

            if total_length is None:
                f.write(r.content)
            else:
                dl = 0
                total_length = int(total_length)
                for data in r.iter_content(chunk_size=4096):
                    dl += len(data)
                    f.write(data)
                    done = int(50 * dl / total_length)
                    sys.stdout.write("\r    - [%s%s]"
                        % ('=' * done, ' ' * (50 - done)))
                    sys.stdout.flush()
            print("")
        else:
            print(Fore.RED + "    - ERROR: Received `%s` when  `%s`" %
                (r.status_code, PATH))
            return False

    return True


def install_bundle(src, dest, backup):
    """Install Bundle

    Install the new application bundle.

    Args:
        src (str): The absolute path to the source (unzipped bundle) folder.
        dest (str): The absolute path to the destination folder.
        backup (str): The absolute path to the backup folder.
    """
    print(Fore.CYAN + "[4] Replace with Updated Bundle")

    try:
        shutil.move(dest, backup)
        print("    - Existing `%s`" % dest)
        print("    - Backup to `%s`" % backup)
    except FileNotFoundError:
        print(Fore.YELLOW + "    - Warning: Destination cannot be backed up, "
            "it does not exist.")

    shutil.move(src, dest)
    print("    - Installing `%s`" % src)
    print("    - Destination `%s`" % dest)


def make_paths():
    """Make Paths

    Make all fo the required paths for the application. This creates folders
    used during the update process.
    """
    print(Fore.CYAN + "[1] Create Required Paths")
    try:
        os.makedirs(PATH, exist_ok=True)
    except FileExistsError:
        print(Fore.RED + "    - ERROR: Path already exsits `%s`" % PATH)


def relaunch(exe, args):
    """Relaunch

    Relaunch the calling applicaition, used both of update, and failure of the
    update.

    Args:
        exe (str): The path of the executable relative to the
    """
    print(Fore.YELLOW + "STUB: Relaunch Application ...")
    sys.exit()


def unzip_bundle(zip, dest):
    """Unzip Bundle

    Unzip the contents of `src` into directory `dest`.

    Args:
        zip (str): The absolute path to the source file to unzip.
        dest (dest): The absolute path of the folder to unzip the files into.

    Returns:
        (bool): Boolean indicating success or failure.
    """
    print(Fore.CYAN + "[3] Unzip Bundle")
    print("    - Zip `%s`" % zip)
    print("    - Destination `%s`" % dest)

    try:
        zf = zipfile.ZipFile(zip, 'r')
        zf.extractall(dest)
        zf.close()
    except zipfile.BadZipFile:
        print(Fore.RED + "    - ERROR: `%s` is not a Zip File!" % zip)
        return False

    return True


if __name__ == '__main__':
    arguments = docopt(__doc__, version='Updater 0.1')
    init(autoreset=True)

    print(Fore.YELLOW + "Upswell Cloud Updater")

    if not arguments.get("URL") or not arguments.get("DEST"):
        print(Fore.RED + "Invalid arguments provided, see `--help`")
        relaunch(arguments.get("exe"), arguments.get("args"))

    PATH = "%sUpdater/" % user_data_dir()
    bundle_uri = arguments.get("URL")
    dest_path = arguments.get("DEST")
    zip_file = bundle_uri.split('/')[-1].split('?')[0]
    zip_path = "%s%s" % (PATH, zip_file)
    unzip_folder = os.path.splitext(zip_file)[0]
    unzip_path = "%s%s" % (PATH, unzip_folder)
    temp_path = "%s~%s" % (PATH, unzip_folder)

    make_paths([PATH])

    if not download_bundle(bundle_uri, zip_path):
        relaunch(arguments.get("exe"), arguments.get("args"))

    if not unzip_bundle(zip_path, unzip_path):
        relaunch(arguments.get("exe"), arguments.get("args"))

    install_bundle(unzip_path, dest_path, temp_path)

    cleanup(zip_path, unzip_path, temp_path)
